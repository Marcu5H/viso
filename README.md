# Viso
Viso is a small, bad, and simple sorting algorithm visualization library for C++ 
written with SLD2.

## Demo videos
### Selection sort
![](demos/selection_sort.mp4)

### QuickSort
![](demos/quick_sort.mp4)
