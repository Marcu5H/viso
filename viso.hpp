#ifndef __VISO_HPP
#define __VISO_HPP 1

#include <SDL2/SDL.h>
#include <string>
#include <vector>

class Viso {
	private:
		SDL_Window* window = NULL;
		SDL_Renderer* renderer = NULL;
		std::string err;
		unsigned int width, height;
	public:
		Viso(unsigned int width, unsigned int height);
		bool init(void);
		void render(unsigned short r, unsigned short g, unsigned short b, int i);
		void cleanup(void);
		std::string getError(void);
	public:
		int arrSize;
		std::vector<int> arr;
};

#endif /* __VISO_HPP */
