#include <iostream>
#include <algorithm>
#include <random>

#include "viso.hpp"

int
main(void)
{
	Viso v(1000, 500);
	if (!v.init()) {
		std::cerr << "Failed to create window: " << v.getError() << '\n';
		exit(1);
	}

	v.arrSize = 250;
	for (int i = 0; i < v.arrSize; i++)
        v.arr.push_back(i);
	std::shuffle(v.arr.begin(), v.arr.end(), std::default_random_engine(0));

	int i = 0, j, min_idx, temp;

	SDL_Event event;
	bool running = true;
	while(running) {
		while(SDL_PollEvent(&event) != 0)
			if(event.type == SDL_QUIT) running = false;

		if (i <= v.arrSize) {
			min_idx = i;
			for (j = i+1; j < v.arrSize; j++) {
				if (v.arr[j] < v.arr[min_idx])
					min_idx = j;
				v.render(255, 255, 255, j);
			}

			if(min_idx!=i) {
				temp = v.arr[min_idx];
				v.arr[min_idx] = v.arr[i];
				v.arr[i] = temp;
			}
			v.render(255, 255, 255, i);
			i++;
		} else {
			v.render(0, 255, 0, -1);
		}

		SDL_Delay(10);
	}

	v.cleanup();

	return 0;
}
