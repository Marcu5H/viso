CXX = g++
CXXFLAGS = -Wall -Wextra -Werror -O2 -std=c++17
CXXLIBS = -lSDL2

OBJS = \
	viso.o

all : $(OBJS) test

%.o : %.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $< $(CXXLIBS)

test : test_selection_sort test_quick_sort

test_selection_sort : test_selection_sort.cpp
	$(CXX) $(CXXFLAGS) -o $@ $< $(CXXLIBS) $(OBJS)

test_quick_sort : test_quick_sort.cpp
	$(CXX) $(CXXFLAGS) -o $@ $< $(CXXLIBS) $(OBJS)

clean :
	$(RM) $(OBJS)
	$(RM) test_selection_sort
	$(RM) test_quick_sort

.PHONY : all test test_selection_sort clean
