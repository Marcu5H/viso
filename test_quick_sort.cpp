/*
 * Quick sort implementation from:
 * https://www.geeksforgeeks.org/quick-sort/?ref=lbp
 */

#include <iostream>
#include <algorithm>
#include <random>

#include "viso.hpp"

static Viso v(1000, 500);

int partition(int low, int high)
{
	int pivot = v.arr[high], temp;
	int i = (low - 1);
	for (int j = low; j <= high - 1; j++) {
		if (v.arr[j] < pivot) {
			i++;
			temp = v.arr[i];
			v.arr[i] = v.arr[j];
			v.arr[j] = temp;
		}
		v.render(255, 255, 255, j);
		SDL_Delay(5);
	}
	temp = v.arr[i + 1];
	v.arr[i + 1] = v.arr[high];
	v.arr[high] = temp;

    return (i + 1);
}

void quickSort(int low, int high)
{
	if (low < high) {
		int pi = partition(low, high);

		quickSort(low, pi - 1);
		quickSort(pi + 1, high);
	}
}

int
main(void)
{
	if (!v.init()) {
		std::cerr << "Failed to create window: " << v.getError() << '\n';
		exit(1);
	}

	v.arrSize = 250;
	for (int i = 0; i < v.arrSize; i++)
        v.arr.push_back(i);
	std::shuffle(v.arr.begin(), v.arr.end(), std::default_random_engine(0));

	int i = 1, low = 0, high = v.arrSize - 1;

	SDL_Event event;
	bool running = true;
	while(running) {
		while(SDL_PollEvent(&event) != 0)
			if(event.type == SDL_QUIT) running = false;

		if (i) {
			quickSort(low, high);
			i = 0;
		} else {
			v.render(0, 255, 0, -1);
		}

		SDL_Delay(10);
	}

	v.cleanup();

	return 0;
}
