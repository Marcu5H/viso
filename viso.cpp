#include <SDL2/SDL.h>
#include <stdio.h>

#include "viso.hpp"

Viso::Viso(unsigned int width, unsigned int height)
{
	this->width = width;
	this->height= height;
}

bool
Viso::init(void)
{
	if(SDL_Init(SDL_INIT_VIDEO) < 0) {
		this->err = SDL_GetError();
    	return false;
    }

    if(!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1")) {
		this->err = SDL_GetError();
    }

    if((this->window = SDL_CreateWindow(
    	"Viso",
    	SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
    	this->width, this->height, SDL_WINDOW_SHOWN)
		) == NULL) {
		this->err = SDL_GetError();
    	return false;
    }

    if((this->renderer = SDL_CreateRenderer(this->window,
									  -1, SDL_RENDERER_ACCELERATED)) == NULL) {
		this->err = SDL_GetError();
        return false;
    }

    SDL_SetRenderDrawColor(this->renderer, 0, 0, 0, 255);

    return true;
}

void
Viso::render(unsigned short r, unsigned short g, unsigned short b, int i)
{
    SDL_RenderClear(this->renderer);

	for (int j = 0; j < this->arrSize; j++) {
		SDL_Rect rect {
			(int) j * ((int)this->width / this->arrSize) + 1,
			(int) this->height,
			(int) this->width / this->arrSize - 1,
			(int) -(this->arr[j] * (this->height / this->arrSize))
		};

		if (j == i) SDL_SetRenderDrawColor(this->renderer, 255, 0, 0, 255);
		else SDL_SetRenderDrawColor(this->renderer, r, g, b, 255);
		SDL_RenderFillRect(this->renderer, &rect);
	}

    SDL_SetRenderDrawColor(this->renderer, 0, 0, 0, 255);

	SDL_RenderPresent(this->renderer);
}

void
Viso::cleanup(void)
{
	if(this->renderer) {
		SDL_DestroyRenderer(this->renderer);
		renderer = NULL;
	}

	if(this->window) {
		SDL_DestroyWindow(this->window);
		window = NULL;
	}

    SDL_Quit();
}

std::string
Viso::getError(void)
{
	return this->err;
}
